#!/usr/bin/env python
"""
Masked wordcloud
================

Using a mask you can generate wordclouds in arbitrary shapes.
"""

from os import path
from PIL import Image
import numpy as np
import matplotlib.pyplot as plt
from wordcloud import WordCloud, STOPWORDS
import requests

d = path.dirname(__file__)
data = requests.get("http://localhost:8000/palavras").json()
palavras = ""
for x in data:
    palavras = palavras + x["palavra"] + " "

# read the mask image
# taken from
# http://www.stencilry.org/stencils/movies/alice%20in%20wonderland/255fk.jpg
alice_mask = np.array(Image.open(path.join(d, "heart.png")))

stopwords = set(STOPWORDS)
stopwords.add("said")

wc = WordCloud(background_color="pink", max_words=2000, mask=alice_mask,
               stopwords=stopwords, colormap="magma")
# generate word cloud
wc.generate(palavras)

# store to file
wc.to_file(path.join(d, "public/image/back.png"))
