<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <style>
            html, body{height:100%;
                margin: 0;
                padding: 0;
                width: 100%;}
            body{
                background-image: url("image/background.png");
                background-size: cover;
                background-repeat: no-repeat;
            }

        </style>
    </head>
    <body>
    <div class="container" style="padding-top: 270px; color: white; text-align: center">
        <h3><b>Obrigado pela sua participação!</b></h3>
        <br>
        <div style="padding-top: 30px">
            <a class="btn btn-success " href="{{route('cadastro')}}">Enviar outra palavra</a>
        </div>
    </div>
    </body>
</html>
