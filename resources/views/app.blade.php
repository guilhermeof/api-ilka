<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Prof. Ilka</title>
    <!-- P5js library -->
    <script src="{{ url('js/p5.min.js') }}"></script>
    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
    <script
            src="https://code.jquery.com/jquery-3.2.1.min.js"
            integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
            crossorigin="anonymous"></script>
    <link href="https://fonts.googleapis.com/css?family=Cabin|Roboto" rel="stylesheet">
</head>
<body>
<script type="text/javascript">

    function getRandomArbitrary(min, max) {
        return parseInt(Math.random() * (max - min) + min);
    }

    /**
     * Classe Word
     * @param str
     */
    function word(str){
        this.word = str;
    }

    word.prototype = new word();

    word.prototype.constructor = word;

    word.prototype.activate = function(){
        this.pos = createVector(random(width),random(height));
        this.col = color(random(255), random(255), random(255));
        this.size = random(60);
        this.time = millis();
        this.trans = random(255);
        this.visible = true;
    };

    word.prototype.draw = function(){
        if(this.visible){
            this.trans = this.trans + (this.time - millis())/77777;
            textSize(this.size);
            fill(color(this.col._getRed(), this.col._getGreen(), this.col._getBlue(), this.trans));
            text(this.word,this.pos.x,this.pos.y);
            if( this.trans < 0 ){
                this.pos = createVector(random(width),random(height));
                this.col = color(random(255), random(255), random(255));
                this.time = millis();
                this.trans = random(255);
                this.visible = true;
            }
        }
    };

    words = [];
    url = "http://localhost:8000/palavras";

    $.ajax({
        url: url,
        data: {
            "_token": "{{ csrf_token() }}"
        },
        method: "GET",
        async: false,
        success: function (data) {
            for(var i = 0; i < data.length; i++){
                words.push(new word(data[i].palavra));
            }
        }
    });

    var poop2 = [];

    function setup() {
        createCanvas(window.innerWidth, window.innerHeight);
        textFont("Roboto");

        var count = (parseInt(width*height/10000)>40)? parseInt(width*height/10000):40;
        for(var i = 0 ; i < count; i++){
            var position = getRandomArbitrary(0, words.length);
            var tmp = words[position];
            tmp.activate();
            poop2.push(tmp);
        }
    }

    sentinel = 0;
    function draw() {
        background(255);

        console.log(sentinel);
        if(sentinel >= 10200){
            window.location.replace("{{ route('cloud') }}");
        }

        $.ajax({
            url: url + "/" + words.length,
            data: {
                "_token": "{{ csrf_token() }}"
            },
            method: "GET",
            async: false,
            success: function (data) {
                for(var i = 0; i < data.length; i++){
                    console.log(data[i].palavra);
                    var palavra = new word(data[i].palavra);
                    palavra.activate();
                    words.push(palavra);
                    poop2.push(palavra);
                }
            }
        });

        for(var i=0; i < poop2.length; i++){
            poop2[i].draw();
            if(poop2[i].trans < 0){
                poop2[i] = poop[parseInt(random(poop.length))];
                poop2[i].activate();
            }
        }
        sentinel++;
    }
</script>
</body>
</html>