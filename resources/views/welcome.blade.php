<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>
        <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body{height:100%;}
            body{
                background-image: url("image/background.png");
                background-size: cover;
                background-repeat: no-repeat;
            }
        </style>
    </head>
    <body>
    <div class="container" style="padding-top: 30px; text-align: center; color: white ">
        <img style="border-radius: 50%; " src="{{ asset('image/ilka_new.png ') }}" alt="">
        <h3 style="padding-top: 20px">Profª Ilka</h3>
    </div>
    <div class="container" style="color: white">
        <form action="{{ route('salvar') }}" method="post">
            <div style="text-align: center" class="form-group">
                <label for="exampleInputEmail1">Informe seu nome</label>
                <input type="text" class="form-control" placeholder="Nome">
                <label for="exampleInputEmail1">Digite abaixo palavras que a definem separado por vírgula</label>
                <input type="text" class="form-control" id="palavra" name="palavra" placeholder="Exemplo: Inteligente, Líder, Amiga">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div style="color: red">
                    @if ($errors->has('palavra')) <p class="help-block">{{ $errors->first('palavra') }}</p> @endif
                    @if ($errors->has('espaco')) <p class="help-block">{{ $errors->first('espaco') }}</p> @endif
                    @if ($errors->has('salvar')) <p class="help-block">{{ $errors->first('salvar') }}</p> @endif
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Enviar</button>
        </form>
    </div>
    </body>
</html>
