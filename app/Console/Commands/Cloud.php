<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class Cloud extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:cloud';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make word clouds';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        chdir(base_path());
        shell_exec('python3 cloud.py > /dev/null 2>/dev/null &');
    }
}
