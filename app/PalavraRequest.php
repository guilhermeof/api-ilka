<?php

namespace App;

use Illuminate\Foundation\Http\FormRequest;

class PalavraRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'palavra' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'palavra.max' => 'A sua palavra deve ter no máximo 25 caracteres',
            'palavra.required' => 'O campo é obrigatório'
        ];
    }
}
