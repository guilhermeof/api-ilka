<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Palavra;
use App\PalavraRequest;

class PalavraController extends Controller
{


    public function save(PalavraRequest $request)
    {
        $palavras = explode(',',  $request->input('palavra'));

        try{
            foreach ($palavras as $value) {
                $tmp = trim($value);

                if (empty($tmp)) {
                    continue;
                }
                $verify = $this->verificaPalavroes($tmp);
                if ($verify){
                    return redirect()->back()->withErrors(['salvar' => 'Que FEIO, NÃO utilize essas palavras!!!']);
                }

                $palavra = new Palavra();
                $palavra->palavra = $tmp;
                $palavra->save();
            }

            return view('success');
        }catch (\Exception $e){
            if (config('app.debug')){
                throw $e;
            }

            return redirect()->back()->withErrors(['salvar' => 'Erro ao tentar salvar, tente novamente']);
        }
    }

    public function form()
    {
        return view('welcome');

    }

    public function getPalavra($numero = 0)
    {
        $palavras = Palavra::where('id', '>', $numero)->get();
        return $palavras;
    }

    public function verificaPalavroes($string){
        $arrayRemover = array( '.', '-', ' ' );
        $arrayNormal = array( "", "", "" );
        $normal = str_replace($arrayRemover, $arrayNormal, $string);


        $string_final = $this->tirarAcentos(strtolower($normal));

        $string_final = strtolower($string_final);
        // Array em Filtro de Palavrões
        $array = array('arrombado',
            'arrombada',
            'buceta',
            'boceta',
            'bocetao',
            'bucetinha',
            'bucetao',
            'bucetaum',
            'blowjob',
            '#@?$%~',
            'caralinho',
            'caralhao',
            'caralhaum',
            'caralhex',
            'c*',
            'cacete',
            'cacetinho',
            'cacetao',
            'cacetaum',
            'penis',
            'vagina',
            'foder',
            'f****',
            'fodase',
            'fodasi',
            'fodassi',
            'fodac',
            'fodassa',
            'fodasse',
            'fodinha',
            'fodao',
            'fodaum',
            'foda1',
            'fodona',
            'f***',
            'fodeu',
            'fodasse',
            'fuckoff',
            'fuckyou',
            'fuck',
            'filhodaputa',
            'filhadaputa',
            'gozo',
            'gozar',
            'gozada',
            'gozadanacara',
            'm*****',
            'merdao',
            'merdaum',
            'merdinha',
            'vadia',
            'vagabunda',
            'vagabundo',
            'vsf',
            'tnc',
            'vtnc',
            'fdp',
            'xiri',
            'vasefoder',
            'venhasefoder',
            'voufoder',
            'vasefuder',
            'venhasefuder',
            'voufuder',
            'vaisefoder',
            'vaisefuder',
            'venhasefuder',
            'vaisifude',
            'v****',
            'vaisifuder',
            'vasifuder',
            'vasefuder',
            'vasefoder',
            'pirigueti',
            'piriguete',
            'p****',
            'porraloca',
            'porraloka',
            'porranacara',
            '#@?$%~',
            'putinha',
            'putona',
            'putassa',
            'putao',
            'punheta',
            'putamerda',
            'putaquepariu',
            'putaquemepariu',
            'putaquetepariu',
            'putavadia',
            'pqp',
            'putaqpariu',
            'putaqpario',
            'putaqparil',
            'peido',
            'peidar',
            'xoxota',
            'xota',
            'xoxotinha',
            'xoxotona',
            'gostosa',
            '10/10',
            'milf',
            'delicia',
            'porra',
            'poha',
            'desgraca',
            'peitao',
            'peitaco',
            'coroa',
            'burra',
            'semvergonha',
            'idiota',
            'pedante',
            'ordinaria',
            'incapaz',
            'chucra',
            'xucra',
            'incompetente',
            'gorda',
            'baleia',
            'balao',
            'elefante',
            'gordinha',
            'gordaca',
            'gordassa',
            'cheinha',
            'caralhuda'
        );

        if(in_array($string_final, $array)){
            return true;
        } else {
            return false;
        }
    }

    function tirarAcentos($string){
        return preg_replace(array("/(á|à|ã|â|ä)/","/(Á|À|Ã|Â|Ä)/","/(é|è|ê|ë)/","/(É|È|Ê|Ë)/","/(í|ì|î|ï)/","/(Í|Ì|Î|Ï)/","/(ó|ò|õ|ô|ö)/","/(Ó|Ò|Õ|Ô|Ö)/","/(ú|ù|û|ü)/","/(Ú|Ù|Û|Ü)/","/(ñ)/","/(Ñ)/"),explode(" ","a A e E i I o O u U n N"),$string);
    }
}
