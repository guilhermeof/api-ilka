<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/cadastro', 'PalavraController@form')->name('cadastro');
Route::get('/palavras/{ok?}', 'PalavraController@getPalavra');
Route::post('/cadastro', 'PalavraController@save')->name('salvar');


Route::get('/app', function (){
    return view('app');
});

Route::get('/cloud', function (){
    return view('cloud');
})->name('cloud');
